# Dotfiles

Elise Shaffer's dotfiles

## Dependencies

1. XCode && XCode Command Line Tools
1. You will need to have your ssh-key attached to BOTH your github and gitlab accounts

## Install

In order to install the dotfiles, clone this repo, then run the following:

```
scripts/install.sh
```

## Other Things

1. Remap Ctrl-R to Ignore in iTerm settings.
1. Download Magnet from the MAS.
1. Install the itermcolors/presets in the itermcolors folder.

Optional:

1. Add `/usr/local/bin/zsh` to `/etc/shells`
1. `chsh -s /usr/local/bin/zsh`
